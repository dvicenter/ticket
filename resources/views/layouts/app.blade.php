<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="stylesheet" type="text/css" href="{{ asset('css/bootstrap.min.css')}}">
	<script type="text/javascript" src="{{ asset('js/bootstrap.min.js') }}"></script>
	<title>Gestion de Ticket.</title>
</head>
<body class="col-xs-9">
@section('title')
	<h1>Gestion de Ticket.</h1>
@show
@yield('content')
</body>
</html>