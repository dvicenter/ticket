@extends('layouts.app')
	@section('title')
		@parent
	@endsection
	@section('content')

		@if(isset($edit))
			@include('layouts/form_ticket')
		@elseif(isset($create))
			@include('layouts/form_ticket')
		@else
			@include('layouts/table_ticket')
		@endif
	@endsection