# Gestion de Ticket

## Instalación

1. Clonar el repositorio en su servidor.
2. Dar permisos al proyecto.
3. Instalar laravel con :  
	
	composer install

4. Crear base de datos dbticket en uft8.
5. Copiar .env.example y cambiarle el nombre a .env.
6. Configurar el .env con los datos de su base de datos local.
	
	DB_CONNECTION=mysql
	DB_HOST=127.0.0.1
	DB_PORT=3306
	DB_DATABASE=dbticket
	DB_USERNAME=root
	DB_PASSWORD=

7. Crear el key del proyecto con el comando:
	
	php artisan key:generate

8. Ejecutar la migracion para crear la tabla :  
	
	php artisan migrate

9. Activar su servidor e ingresar al proyecto.