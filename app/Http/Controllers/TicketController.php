<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Ticket;

class TicketController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ticket = Ticket::all();
        return view('home')->with(['tickets'=> $ticket]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("home")->with('create',true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'nombre' => 'required',
            'descripcion' => 'required',
            'importancia' => 'required',
        ]); 

        $ticket = new Ticket();
        $ticket->nombre = $request->nombre;
        $ticket->descripcion = $request->descripcion;
        $ticket->importancia = $request->importancia;

        if ($ticket->save()) {
            return redirect('/');
        } else {
            return back()->with('errormsj', 'Los datos no se guardaron');  
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::find($id);
        return view('home')->with(['edit'=>true, 'ticket'=>$ticket]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $this->validate($request, [
            'nombre' => 'string',
            'descripcion' => 'string',
        ]);

        $ticket = Ticket::find($id);
        $ticket->descripcion = $request->descripcion;
        $ticket->importancia = $request->importancia;

        if ($ticket->save()) {
            return redirect('/');
        } else {
            return back()->with('errormsj', 'Los datos no se actualizaron');  
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Ticket::destroy($id);
        return redirect('/');
    }

    public function search(Request $request)
    {

        $ticket = Ticket::where('nombre','like','%'.$request->search.'%')->orWhere('descripcion','like','%'.$request->search.'%')->get();

       return view('home', ['buscado'=>true,'tickets' => $ticket]);
    }

    public function filter(Request $request)
    {   
        $ticket = Ticket::where('importancia',(int)$request->importancia)->get();

        return view('home', ['filtrado'=>true, 'tickets' => $ticket]);
    }
}
