		<form  class="form-inline" method="POST" action="{{ url('ticket/search') }}">
			{{ csrf_field() }}
			<input type="text" name="search" class="form-control" />
			<input type="submit" value="buscar" class="btn" />
		</form> <br>
		<form method="POST" action="{{ url('ticket/filter') }}">
			{{ csrf_field() }}
			 <div class="col-xs-2">
				<select class="form-control" name="importancia">
					<option value="0" >Urgente</option>
					<option value="1" >Medio</option>
					<option value="2" >Bajo</option>
				</select>
			</div>
			<input type="submit" value="filtrar" class="btn" />
		</form> <br>

<a href="{{ url('ticket/create') }}" class="btn btn-primary">Create</a> <br>
<table class="table table-hover">
	@if(isset($tickets))
		<thead >
			<tr>
				<th>Nombre</th>
				<th>Descripcion</th>
				<th>Importancia</th>
				<th>Fecha</th>
				<th>Acciones</th>
			</tr>
		</thead>
		<tbody>
			@foreach($tickets as $row)
			<tr>
				<td>{{$row->nombre}}</td>
				<td>{{$row->descripcion}}</td>
				<td>
					@if ($row->importancia == 0)
						Urgente
					@elseif ($row->importancia == 1)
					    Medio
					@elseif ($row->importancia == 2)
					    Bajo
					@endif
				</td>
				<td>{{$row->created_at}}</td>
				<td>
					<a href="{{ url('ticket/'.$row->id.'/edit') }}" class="btn btn-warning btn-xs">Modifica</a> 
					<form method="POST" action="{{ route('ticket.destroy', $row->id) }}">
						<input name="_method" type="hidden" value="DELETE">
						{{ csrf_field() }}
						<input type="submit" class="btn btn-danger btn-xs" value="Eliminar" />
					</form>
				</td>
			</tr>
			@endforeach
		</tbody>
	@endif
</table>