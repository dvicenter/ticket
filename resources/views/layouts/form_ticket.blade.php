<?php
	$nombre = isset($ticket->nombre)? $ticket->nombre : '' ;
	$descripcion = isset($ticket->descripcion)? $ticket->descripcion : '' ;
	$importancia = isset($ticket->importancia)? $ticket->importancia : '' ;
	$create_at = isset($ticket->create_at)? $ticket->create_at : '' ;
?>
@if(isset($edit))
<form class="form-horizontal" method="post" action="{{ route('ticket.update', $ticket->id) }}">
<input name="_method" type="hidden" value="PUT">

	{{ csrf_field() }}
	<div class="form-group">
	    <label for="name" class="col-sm-2 control-label">Nombre</label>
	    <div class="col-sm-3">
			<input id="name" type="text" class="form-control" name="nombre" value="{{$nombre}}" {{ isset($edit) ? 'disabled' : "" }}/>
		</div>
	</div>
	<div class="form-group">
	    <label for="des" class="col-sm-2 control-label">Descripcion</label>
	    <div class="col-sm-3">
			<textarea id="des" class="form-control" name="descripcion">{{$descripcion}}</textarea>
		</div>
	</div>
	<div class="form-group">
	    <label for="imp" class="col-sm-2 control-label">Importancia</label>
	    <div class="col-sm-3">
			<select id="imp" class="form-control" name="importancia">
				<option value="0" {{ ($ticket->importancia==0) ? 'selected="selected"' : "" }}>Urgente</option>
				<option value="1" {{ ($ticket->importancia==1) ? 'selected="selected"' : "" }}>Medio</option>
				<option value="2" {{ ($ticket->importancia==2) ? 'selected="selected"' : "" }}>Bajo</option>
			</select>
		</div>
	</div>
	<div class="form-group">
	    <label for="dat" class="col-sm-2 control-label">Fecha</label>
	    <div class="col-sm-3">
			<input id="dat" class="form-control" type="date" name="create_at" value="{{$create_at}}"  {{ isset($edit) ? 'disabled' : "" }}/> 
		</div>
	</div>
	<input type="submit" class="btn btn-primary" value="modificar" />
	<a href="{{ url('/') }}" class="btn">Atras</a>
</form>
@elseif(isset($create))
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form class="form-horizontal" method="post" action="{{ url('ticket') }}">

	{{ csrf_field() }}
	<div class="form-group">
	    <label for="name" class="col-sm-2 control-label">Nombre</label>
	    <div class="col-sm-3">
			<input id="name" type="text" class="form-control" name="nombre" />
		</div>
	</div>
	<div class="form-group">
	    <label for="des" class="col-sm-2 control-label">Descripcion</label>
	    <div class="col-sm-3">
			<textarea id="des" class="form-control" name="descripcion" ></textarea>
		</div>
	</div>
	<div class="form-group">
	    <label for="imp" class="col-sm-2 control-label">Importancia</label>
	    <div class="col-sm-3">
			<select id="imp" class="form-control" name="importancia" >
				<option value="0">Urgente</option>
				<option value="1">Medio</option>
				<option value="2">Bajo</option>
			</select>
		</div>
	</div>
	<div class="form-group">
	    <label for="dat" class="col-sm-2 control-label">Fecha</label>
	    <div class="col-sm-3">
			<input id="dat" class="form-control" type="date" name="create_at" value="{{Carbon\Carbon::today()->format('Y-m-d')}}" disabled />
		</div>
	</div>
	
	<input type="submit" class="btn btn-primary" value="crear" />
	<a href="{{ url('/') }}" class="btn">Atras</a>
</form>
@endif